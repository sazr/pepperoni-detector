#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>

using namespace cv;


const char* keys =
{
	"{help h usage ?| | Production line quality assurance using image processing }"
};


static bool withinRunningAverage(const Mat& image, Mat& runningAvg, double alpha = 0.3, double thresh = 34.0);
static bool withinBinaryAverage(const Mat& src, Scalar& prevMean, double thresh = 30.0);
static void detectPizzas(const Mat& roi);
static void detectPepperoniUsingFrequency(Mat& image, /*const*/ Mat& pizzaRoi);
static void detectPepperoniUsingHue(Mat& image, /*const*/ Mat& pizzaRoi);
static void changeLighting(Mat& image, double intensityReduction = 0.5);


int main(int argc, char** argv)
{
	CommandLineParser parser(argc, argv, keys);

	if (parser.has("help")) {
		parser.printMessage();
		return 0;
	}

	VideoCapture capture("feed.mp4");

	if (!capture.isOpened()) {
		printf("Failed to open video stream\n");
		return 0;
	}

	Scalar prevMean;
	Mat frame, roi, pizzaDetectionTrigger, runningAvg, diff;
	Rect roiRect(60, 240, 460, 90);

	int nFrames = capture.get(CV_CAP_PROP_FRAME_COUNT);

	while (1) {

		if (!capture.read(frame))
			goto Cleanup;

		// Extract 'Pizza Detection Trigger' (roi 1 pixel in height and width of frame): used to produce running average
		roi = frame(roiRect);
		pizzaDetectionTrigger = frame({ roiRect.x, roiRect.y + int(roiRect.height * 0.85), roiRect.width, 1 });
		cvtColor(pizzaDetectionTrigger, pizzaDetectionTrigger, CV_BGR2HSV);
		
		imshow("roi", roi);

		// If the roi mean colour differs from the running average we have detected the bottom of the pizzas
		if (!withinRunningAverage(pizzaDetectionTrigger, runningAvg))
		{
			detectPizzas(roi);

			pizzaDetectionTrigger.convertTo(runningAvg, CV_32FC1);
		}

		if (waitKey(30) == 27)
			goto Cleanup;
	}

Cleanup:
	capture.release();
	return 0;
}

static void detectPizzas(const Mat& roi)
{
	bool pizzasFound = false;
	Mat blur, edges;
	GaussianBlur(roi, blur, Size(3, 3), 0);
	Canny(blur, edges, 325, 350);
	dilate(edges, edges, getStructuringElement(CV_SHAPE_ELLIPSE, { 3, 3 }), { -1, -1 }, 3);

	std::vector<std::vector<Point>> contours;
	findContours(edges, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	for (int i = 0; i < contours.size(); i++) 
	{
		RotatedRect pizzaEllipse = fitEllipse(contours[i]);

		std::vector<Point2f> ellipsePnts(4);
		pizzaEllipse.points(&ellipsePnts[0]);

		if (contourArea(ellipsePnts) < 5000)
			continue;

		pizzasFound = true;
		Rect pizzaBounds = boundingRect(contours[i]);
		Mat pizzaRoi = roi(pizzaBounds);

		detectPepperoniUsingFrequency(edges, pizzaRoi);
		detectPepperoniUsingHue(edges, pizzaRoi);

		ellipse(edges, pizzaEllipse, { 125 }, 2);
	}

	if (pizzasFound)
		waitKey(0);
}

static void detectPepperoniUsingFrequency(Mat& image, /*const*/ Mat& pizzaRoi)
{
	Mat edges, clone;
	pizzaRoi.copyTo(clone);
	Canny(pizzaRoi, edges, 0, 25);
	//dilate(edges, edges, getStructuringElement(CV_SHAPE_ELLIPSE, { 3, 3 }), { -1, -1 }, 1);

	std::vector<std::vector<Point>> contours;
	findContours(edges, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	for (int i = 0; i < contours.size(); i++)
	{
		if (contours[i].size() < 5)
			continue;

		RotatedRect pizzaEllipse = fitEllipse(contours[i]);

		std::vector<Point2f> ellipsePnts(4);
		pizzaEllipse.points(&ellipsePnts[0]);
		double area = contourArea(ellipsePnts);

		if (area < 150 || area > 1000)
			continue;

		ellipse(clone, pizzaEllipse, { 0, 0, 255 }, 2);
	}

	imshow("Frequency Result", clone);
}

static void detectPepperoniUsingHue(Mat& image, /*const*/ Mat& pizzaRoi)
{
	Mat hsv, clone;
	pizzaRoi.copyTo(clone);

	cvtColor(clone, hsv, CV_BGR2HSV);

	Mat thresh;
	inRange(hsv, Scalar(0, 105, 0), Scalar(30, 135, 255), thresh);

	erode(thresh, thresh, getStructuringElement(CV_SHAPE_ELLIPSE, { 5, 5 }));
	
	std::vector<std::vector<Point>> contours;
	findContours(thresh, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	for (int i = 0; i < contours.size(); i++)
	{
		if (contours[i].size() < 5)
			continue;

		RotatedRect pizzaEllipse = fitEllipse(contours[i]);
		
		std::vector<Point2f> ellipsePnts(4);
		pizzaEllipse.points(&ellipsePnts[0]);

		//if (contourArea(ellipsePnts) < 30)
		//	continue;

		ellipse(clone, pizzaEllipse, { 125 }, 2);
	}

	imshow("Spatial/Hue Result", clone);
}

static bool withinRunningAverage(const Mat& src, Mat& runningAvg, double alpha, double thresh)
{
	if (runningAvg.empty())
		src.convertTo(runningAvg, CV_32FC1);

	accumulateWeighted(src, runningAvg, alpha);

	Scalar curAvg = mean(src);
	Scalar genAvg = mean(runningAvg);

	//printf("Diff: %f\n", norm(genAvg, curAvg, CV_L2));

	return norm(genAvg, curAvg, CV_L2) < thresh;
}

static bool withinBinaryAverage(const Mat& src, Scalar& prevMean, double thresh)
{
	Scalar curMean = mean(src);

	double diff = norm(curMean, prevMean, CV_L2);

	//printf("Diff: %f\n", diff);

	prevMean = curMean;

	return diff < thresh;
}

static void changeLighting(Mat& image, double intensityReduction)
{
	Mat hsv, darkened;
	cvtColor(image, hsv, CV_BGR2HSV);

	std::vector<Mat> channels;
	split(hsv, channels);

	channels[2] *= intensityReduction;
	merge(channels, darkened);
	cvtColor(darkened, image, CV_HSV2BGR);
}